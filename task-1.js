//simple way without using fetch
// function getInput() {
//     var nInput = document.getElementById("input").value;
    
//     setInterval(function () {
//         var response = "https://source.unsplash.com/random/";
        
//         clearInterval(response);
//         document.getElementById("random-img").src = response + Math.random();
//         console.log(nInput);
//     }, nInput);
// }


//using fetch
function getInput() {
    var nInput = document.getElementById("input").value;
    
    setInterval(() => {
        var url = 'https://dog.ceo/api/breeds/image/random';
        fetch(url)
        .then((response) => {
            return response.json(); //json will parse the data
        })
        .then((actualData) => {
            clearInterval(actualData); 
            document.getElementById("apiImg").src = actualData.message;
            return actualData;
        })
        .catch((error) => {
            console.log(error);
        })
    }, nInput * 1000);

}


